import { DateTime } from 'luxon'
import { BaseModel, column, ManyToMany, BelongsTo, manyToMany, belongsTo } from '@ioc:Adonis/Lucid/Orm'
import User from 'App/Models/users'
import Field from 'App/Models/Field'

/** 
*  @swagger
*  definitions:
*    Booking:
*      type: object
*      properties:
*        id:
*          type: integer
*        title:
*          type: string
*        play_date_start:
*          type: string
*        play_date_end:
*          type: string
*        user_id:
*          type: integer
*        field_id:
*          type: integer
*      required:
*        - field_id
*        - play_date_start
*        - play_date_end
*
*/

export default class Booking extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public title: string

  @column.dateTime({
    serialize: (value: DateTime | null) =>{
      return value ? value.setLocale('id-US').toLocaleString({year: 'numeric', month: '2-digit', day: 'numeric', hour: '2-digit', minute:'2-digit', second: '2-digit'}) : value
    },
  })
  public playDateStart: DateTime

  @column.dateTime({
    serialize: (value: DateTime | null) =>{
      return value ? value.setLocale('id-US').toLocaleString({year: 'numeric', month:'2-digit', day: '2-digit',hour: '2-digit', minute: '2-digit', second: '2-digit'}) : value
    },
  })
  public playDateEnd: DateTime

  @column()
  public userId: number

  @column()
  public fieldId: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(()=> Field)
  public field: BelongsTo<typeof Field>

  @manyToMany(()=> User,{
    pivotTable: 'schedules'
  })
  public player: ManyToMany <typeof User>
}
