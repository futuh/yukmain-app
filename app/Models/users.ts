import { DateTime } from 'luxon'
import { BaseModel, column, ManyToMany, manyToMany, hasMany, HasMany, beforeSave } from '@ioc:Adonis/Lucid/Orm'
import Booking from 'App/Models/Booking'
import Hash from '@ioc:Adonis/Core/Hash'

export default class User extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public email: string

  @column({ serializeAs: null })
  public password: string

  @column()
  public rememberMeToken?: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column()
  public isVerified: boolean

  @column()
  public role: string

  @beforeSave()
  public static async hashPassword (user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
    }
  }

  @hasMany(()=> Booking)
  public myBookings: HasMany <typeof Booking>

  @manyToMany(()=> Booking,{
    pivotTable: 'schedules'
  })
  public player: ManyToMany <typeof Booking>
}
