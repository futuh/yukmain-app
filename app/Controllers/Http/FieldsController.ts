import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database';
import Venue from 'App/Models/Venue';
import Field from 'App/Models/Field';

export default class FieldsController {
    public async getData(id: number){
        const data = await Database.query().select('fields.id', 'fields.name', 'fields.type', 'fields.venue_id', 'venues.name', 'venues.address', 'venues.phone')
        .from('fields').leftJoin('venues','fields.venue_id','venues.id').where('fields.venue_id',id)
      return data;
    }
   public async index({params, response}: HttpContextContract){
       const data = await this.getData(params.venue_id);
       return response.status(200).json({ message: 'success get fields', data})
       
   }
   public async store({ request, response, params }: HttpContextContract ){
           const venue = await Venue.findByOrFail('id', params.venue_id)
           const newFieldId = new Field()
           newFieldId.name = request.input('name')
           newFieldId.tipe = request.input('type')
           await newFieldId.related('venue').associate(venue)

           return response.created({message: 'new field created!', data: newFieldId})
   }
   public async show({response, params}: HttpContextContract){
       let field = await Database.query().select('venues.id', 'venues.name', 'venues.address', 'venues.phone', 'fields.name', 'fields.type')
       .from('venues').leftJoin('fields','venues.id','fields.venue_id').where('venues.id', params.venue_id).where('fields.id', params.id).firstOrFail()
       
       response.ok({message: 'success get field by id', data: field})
   }
   public async update({request, response, params}: HttpContextContract){
       let id = params.id
       await Database.from('fields').where('id', id).select('id', 'name', 'type', 'venue_id').update({
           name: request.input('name'),
           type: request.input('type'),
           venue_id: request.input('venue_id')
       })
       response.ok({message: 'data field has been updated'})
   }
   public async destroy({params, response}: HttpContextContract){
       await Database.from('fields').where('id', params.id).delete()
       response.ok({message: 'data has been deleted'})
   }

   public async fieldBooked({params, response}: HttpContextContract){
       let field = await Field.query().where('id',params.id).select('venueId','name','type','id')
       .preload('venue', (venueQuery) => {
           venueQuery.select('name', 'address', 'phone')
       }).preload('booking', (bookingQuery) =>{
           bookingQuery.select('title','play_date_start','play_date_end').where('user_id', params.id)
       }).firstOrFail()
       
       return response.ok({message: 'berhasil get data booking', data: field})
   }
}
