import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Field from 'App/Models/Field'
import Booking from 'App/Models/Booking'
import BookingValidator from 'App/Validators/BookingValidator'
import User from 'App/Models/users'

export default class BookingsController {
    
    public async index({response}: HttpContextContract ){
        const book = await Booking.query().select('id','user_id','field_id','play_date_start','play_date_end').preload('field',(query)=>{
            query.select('id','name','tipe','venue_id').preload('venue',(query)=>{
                query.select('id','name','address','phone')
            })
        }).preload('player',(query)=>{
            query.select('id','name')
        })

        response.ok({message: 'berhasil get data booking', data: book})
    }
    public async store({ auth, response, params, request }: HttpContextContract ){
    
        const field = await Field.findByOrFail('id', params.field_id)
        const user = auth.user!

      const payload = await request.validate(BookingValidator)
      const newBooking = new Booking()
      newBooking.playDateStart = payload.play_date_start
      newBooking.playDateEnd = payload.play_date_end

      newBooking.related('field').associate(field)
      user.related('myBookings').save(newBooking)
      user.related('player').save(newBooking)
      
      return response.created({status: 'new booking created!', data: newBooking})
    }
    public async show({params,response}: HttpContextContract){
    
        let booking = await Booking.query().where('id',params.id).select('id','field_id','play_date_start','play_date_end','user_id')
        .preload('player',(query)=>{
           query.select('id','name','email')
        }).withCount('player',(query2)=>{
            query2.as('player_count')
        }).firstOrFail()
        let player_count = booking.$extras.player_count
        
        return response.ok({message: 'berhasil get data booking', data: {booking,player_count}})
    }
    public async update({}: HttpContextContract ){
      
    }
    public async destroy({}: HttpContextContract ){
      
    }
    public async join({params, auth,response}:HttpContextContract){
        //const bookJoin = await Booking.findByOrFail('id', params.id)
        
        const newJoin = await Booking.findByOrFail('id',params.id)
        
        const user = auth.user!
        if (newJoin.userId !== user.id){
        await newJoin.related('player').save(user,true)

        response.ok({message: 'berhasil join booking'})
        }else{
        response.status(400).json({message: 'Duplicate entries'})    
        }
        
    }
    public async unjoin({params, auth,response}:HttpContextContract){

        //const bookJoin = await Booking.findByOrFail('id', params.id)
        
        const newJoin = await Booking.findByOrFail('id',params.id)
        const user = auth.user!

        newJoin.related('player').detach([user.id])

        return response.ok({message: 'berhasil unjoin booking'})

    }
    public async getSchedule({ auth, response }: HttpContextContract){

        let user = auth.user!

        const schedule = await User.query().where('id', user.id).select('id','name').preload('myBookings',(query)=>{
            query.select('id','user_id','play_date_start','play_date_end','field_id').preload('field',(query)=>{
                query.select('id','name','tipe','venue_id').preload('venue',(query)=>{
                    query.select('id','name','address','phone')
                    })
                })
            }
        )
            return response.ok({message: 'berhasil get schedule', data: schedule})
    }
}
