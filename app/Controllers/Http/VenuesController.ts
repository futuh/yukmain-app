import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import Booking from 'App/Models/Booking'
import Field from 'App/Models/Field'
import Venue from 'App/Models/Venue'
import VenueValidator from 'App/Validators/VenueValidator'
import BookingValidator from 'App/Validators/BookingValidator'

export default class VenuesController {

    public async index({response}: HttpContextContract){
        
        let venues =await Database.from('venues').select('id', 'name', 'address', 'phone')
        return response.status(200).json({ message: 'success get venues', data: venues})

    }
    public async store({ request, response }: HttpContextContract ){

            let newVenue = new Venue;
            newVenue.name = request.input('name')
            newVenue.address = request.input('address')
            newVenue.phone = request.input('phone')
            await newVenue.save()

            response.created({message: 'new venue created!', data: newVenue})
        
    }
    public async show({params, response}: HttpContextContract){
        //let venue = await Database.from('venues').where('id', params.id).select('id', 'name', 'address', 'phone').firstOrFail()
        // let venue = await Venue.query().where('id', params.id).preload('fields').firstOrFail()
        // response.ok({message: 'success get contact by id', data: venue})
        let today = new Date()
        //let dateTime = today.toLocaleString('en-ID')
        let tommorow = new Date(today)
        tommorow.setDate(tommorow.getDate() + 1)
        //let dateTime2 = tommorow.toLocaleString('en-ID')
        const venue = await Venue.query().where('id', params.id).select('id', 'name', 'address', 'phone')
        .preload('fields',(query)=>{
            query.select('id','name','tipe','venue_id').preload('booking',(query) => {
                query.select('id','field_id','play_date_start','play_date_end').whereBetween('play_date_start', [today, tommorow])
            }).first()
        })
        //const field = await Field.findByOrFail('venues_id', venue.id )
        //const book = await Booking.findByOrFail('fields_id', field.id)
        return response.status(200).json({ message: 'success get venue', data: venue})
    }
    public async update({request, response, params}: HttpContextContract){

        await request.validate(VenueValidator);
        let id = params.id
        await Database.from('venues').where('id', id).select('id', 'name', 'address', 'phone').update({
            name: request.input('name'),
            address: request.input('address'),
            phone: request.input('phone')
        })
        response.ok({message: 'data venue has been updated'})
    }
    public async destroy({params, response}: HttpContextContract){
        await Database.from('venues').where('id', params.id).delete()
        response.ok({message: 'data has been deleted'})
    }
    public async bookVenue({params, response, request, auth}: HttpContextContract){
        const field = await Field.findByOrFail('venue_id', params.id)
        
        const user = auth.user!


        const payload = await request.validate(BookingValidator)
        let newBook = new Booking()
        newBook.fieldId = request.input('field_id')
        newBook.playDateStart = payload.play_date_start
        newBook.playDateEnd = payload.play_date_end
        await newBook.related('field').associate(field)
        user.related('player').save(newBook)
        user.related('myBookings').save(newBook)


        response.ok({message: 'Berhasil booking'})

    }
}
