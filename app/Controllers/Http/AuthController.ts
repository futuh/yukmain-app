import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/users'
import RegisterValidator from 'App/Validators/RegisterValidator'
import { schema } from '@ioc:Adonis/Core/Validator'
import Mail from '@ioc:Adonis/Addons/Mail'
import Database from '@ioc:Adonis/Lucid/Database'

export default class AuthController {

  public async register ({request, response}: HttpContextContract){
    const payload = await request.validate(RegisterValidator)

    const newUser= await User.create({
      name: payload.name,
      email: payload.email,
      password: payload.password,
      role: payload.role
    })
    const otp_code = Math.floor(100000 + Math.random()*900000)
    await Database.table('otp_codes').insert({otp_code: otp_code, user_id: newUser.id})
    await Mail.send((message) => {
      message
        .from('futuhululazmi@gmail.com')
        .to(newUser.email)
        .subject('Welcome Onboard!')
        .htmlView('emails/otp_confirmation', { otp_code })
    })
    
    return response.created({status: 'new user registered, please verify account check your mail', data: newUser})
  }

  public async login ({auth, request, response}: HttpContextContract){
    let email = request.input('email')
    let user = await User.findBy('email', email)
    if (user?.isVerified == true){
    const loginSchema = schema.create({
      email: schema.string({trim: true}),
      password: schema.string({trim: true})
    })
    const payload = await request.validate({schema: loginSchema})
    const token = await auth.use('api').attempt(payload.email, payload.password)
    return response.ok({status: 'success', data: token})
  }else{
    return response.status(400).json({message: 'please verify account check your email'})
  }
  }
  
  public async otpVerification ({ request, response }: HttpContextContract) {
    let otp_code = request.input('otp_code')
    let email = request.input('email')

    let user = await User.findBy('email', email)
    let otpCheck = await Database.query().from('otp_codes').where('otp_code', otp_code).firstOrFail()

    if (user?.id == otpCheck.user_id){
      user!.isVerified = true
      await user?.save()
      return response.status(200).json({message: 'Berhasil konfirmasi otp'})
    }else{
      return response.status(400).json({message: 'gagal konfirmasi otp'})
    }
  }
}
