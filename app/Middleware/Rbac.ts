import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class Rbac {
  //Role Based Access Control
  public async handle ({auth}: HttpContextContract, next: () => Promise<void>, rules) {
    // code for middleware goes here. ABOVE THE NEXT CALL
    const roles = rules
    let user = auth.user
    let roleUser = user?.role
    if(roleUser?.includes(roles)){
    await next()
    }else if(roleUser == 'admin'){
    await next()
    }else{
      throw new Error(`Only user with role: ${roles} and admin can access this route`)
    }
  }
}
