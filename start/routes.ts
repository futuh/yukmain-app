/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})
Route.group(()=>{
Route.post('/login', 'AuthController.login').as('auth.login')
Route.post('/register', 'AuthController.register').as('auth.register')
Route.post('/otp-verification', 'AuthController.otpVerification').as('auth.otpVerification')
Route.get('/venues','VenuesController.index').middleware(['auth']).as('getAllVenue')
Route.get('/venues/:id','VenuesController.show').middleware(['auth']).as('getFieldtoBook')
Route.post('/venues','VenuesController.store').middleware(['auth','rbac:owner']).as('registerVenue')
Route.put('/venues/:id','VenuesController.update').middleware(['auth','rbac:owner']).as('updateVenue')
Route.post('/venues/:id/bookings', 'VenuesController.bookVenue').middleware(['auth','rbac:user']).as('bookVenue')
Route.get('/bookings','BookingsController.index').middleware('auth').as('getBooking')
Route.get('/bookings/:id','BookingController.show').middleware('auth').as('getBookingToday')
Route.put('/bookings/:id/join', 'BookingsController.join').middleware(['auth','rbac:user']).as('joinBooking')
Route.put('/bookings/:id/unjoin', 'BookingsController.unjoin').middleware(['auth','rbac:user']).as('cancelJoin')
Route.get('/schedules', 'BookingsController.getSchedule').middleware(['auth','rbac:user']).as('getSchedule')
Route.post('/fields','FieldsController.store').middleware(['auth','rbac:owner']).as('RegisterNewField')
}).prefix('api/v1')
