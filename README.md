## Name
<h3>Aplikasi Main Bersama</h3>
Final project JCC 2021 Node Js Adonis 

## Description
Aplikasi Main Bareng adalah aplikasi yang dikembangkan dengan tujuan untuk mempertemukan pemuda-pemudi yang ingin berolahraga tim (futsal/voli/basket/minisoccer/soccer) tetapi kekurangan pemain. Aplikasi ini akan menjembatani agar perorangan maupun kelompok yang kekurangan pemain seperti soccer yang jumlah pemainnya minimum 11 orang x 2 tim, agar dapat menemukan pemain dan memenuhi kekurangan jumlah pemain.

User dapat melakukan pemesanan booking baru (create), lalu update, read dan delete Booking tersebut.  Satu Booking bisa diikuti oleh banyak user.
User dapat join dan/atau cancel ke banyak Booking.
owner dapat membuat (create), melihat (read) dan mengubah (update) data venue. 
Membuat dokumentasi API
Deploy aplikasi API (heroku)

## Contributing
Terimakasih kepada panitia JCC 2021 terutama para trainer JCC 2021 Backend NodeJs Adonis terimakasih sudah membimbing selama 1 bulan ini dan kepada semua peserta JCC 2021 terutama peserta JCC 2021 Backend NodeJs Adonis Batch 1

## Authors
Futuh Ulul Azmi
email: futuhululazmi@gmail.com

## Link Deploy Heroku
https://olahragabareng.herokuapp.com/docs/index.html


