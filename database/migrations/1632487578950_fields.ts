import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Fields extends BaseSchema {
  protected tableName = 'fields'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.string('name').notNullable
      table.enum('type',['soccer','futsal','mini soccer','basketball','volleyball']).defaultTo('futsal')
      table.integer('venue_id', 25).unsigned().references('id').inTable('venues').onDelete('CASCADE')

      table.timestamps(true, true)
      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      //table.timestamp('created_at', { useTz: true })
      //table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
